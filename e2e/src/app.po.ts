import { browser, by, element, ElementArrayFinder, ElementFinder, WebElement, $$ } from 'protractor';

export class AppPage {

  static jobTitleLocator = "//app-job/h1";
  static jobLocationLocator = "//app-job/p";
  static jobApplyLocator = "//app-job/button";
  static jobInfoLocator = "//app-job/";
  static applicantNameLocator = "//input[@name='name']";
  static applicantEmailLocator = "//input[@name='email']";
  static applicantResumeLocator = "//input[@name='resume']";
  static jobBoardLocator = "//ul/li";

  navigateTo(): Promise<unknown> {
    return browser.get(browser.baseUrl) as Promise<unknown>;
  }

  getBrowseHeaderText(): Promise<string> {
    return element(by.xpath("//app-jobs/h1")).getText() as Promise<string>;
  }

  getTechnologyHeaderText(): Promise<string> {
    return element(by.xpath("//app-jobs/h2")).getText() as Promise<string>;
  }

  getJobBoardListCount(): Promise<number> {
    return element.all(by.xpath(AppPage.jobBoardLocator)).count() as Promise<number>;
  }

  getJobTitleText(): Promise<string> {
    return element(by.xpath(AppPage.jobTitleLocator)).getText() as Promise<string>;
  }

  getJobLocationText(): Promise<string> {
    return element(by.xpath(AppPage.jobLocationLocator)).getText() as Promise<string>;
  }

  validateJobTableCellNotEmpty(): void{
    
    element.all(by.xpath(AppPage.jobBoardLocator + "//a/span")).each(function(element, index) {
      element.getText().then(function (text) {
        expect(text).not.toEqual("");
      });
    });
    
  }

  validateEachJobAppliedSuccessfully(): void{

    var jobTitles = [], jobLocations = [];

    element.all(by.xpath(AppPage.jobBoardLocator + "//a")).each(function(element, index) {
      
      let jobTitle, jobLocation;
      
      element.$("span.title").getText().then(function (text){
        jobTitle = text;
        expect(jobTitle).not.toEqual("");
        jobTitles.push(jobTitle);
      })

      element.$("span.location").getText().then(function (text){
        jobLocation = text;
        expect(jobLocation).not.toEqual("");
        jobLocations.push(jobLocation.replace("Location: ",""));
      })

    }).then(function (){
      

      for(var i=0; i < jobTitles.length; i++){
        
        $$("ul li a").get(i).click();
        
        expect(element(by.xpath(AppPage.jobTitleLocator)).getText()).toEqual(jobTitles[i].toUpperCase());
        expect(element(by.xpath(AppPage.jobLocationLocator)).getText()).toEqual(jobLocations[i]);
        
        expect(element(by.xpath(AppPage.jobApplyLocator)).getText()).toEqual("APPLY NOW");
        expect(element(by.xpath(AppPage.jobInfoLocator + "h2[1]")).getText()).toEqual("DETAILED ROLE DESCRIPTION:");
        expect(element(by.xpath(AppPage.jobInfoLocator + "ul[1]")).getText()).not.toEqual("");
        expect(element(by.xpath(AppPage.jobInfoLocator + "h2[2]")).getText()).toEqual("DESIRED SKILLS:");
        expect(element(by.xpath(AppPage.jobInfoLocator + "ul[2]")).getText()).not.toEqual("");
        expect(element(by.xpath(AppPage.jobInfoLocator + "h2[3]")).getText()).toEqual("WHY YOU DO NOT WANT TO MISS THIS CAREER OPPORTUNITY?");
        expect(element(by.xpath(AppPage.jobInfoLocator + "ul[3]")).getText()).not.toEqual("");

        element(by.xpath(AppPage.jobApplyLocator)).click();
        
        expect(element(by.xpath(AppPage.applicantNameLocator + "/preceding-sibling::label")).getText()).toEqual("Name*");
        expect(element(by.xpath(AppPage.applicantNameLocator)).getAttribute("placeholder")).toEqual("Name");
        element(by.xpath(AppPage.applicantNameLocator)).sendKeys("test name");
        expect(element(by.xpath(AppPage.applicantEmailLocator + "/preceding-sibling::label")).getText()).toEqual("Email*");
        expect(element(by.xpath(AppPage.applicantEmailLocator)).getAttribute("placeholder")).toEqual("Please enter your email");
        element(by.xpath(AppPage.applicantEmailLocator)).sendKeys("test@test.com");
        expect(element(by.xpath(AppPage.applicantResumeLocator + "/preceding-sibling::label")).getText()).toEqual("Attach Resume");
        element(by.xpath(AppPage.applicantResumeLocator)).sendKeys(process.cwd() + "/e2e/Arpit Latest CV.pdf");
        browser.actions().mouseMove(element(by.xpath(AppPage.jobApplyLocator))).click();
        
        browser.navigate().back();
      }
      
      
      
      
    });

  
  }
}
