import { AppPage } from './app.po';
import { browser, logging, WebElement, by, element } from 'protractor';

describe('workspace-project App', () => {
  let page: AppPage;

  beforeEach(() => {
    page = new AppPage();
  });

  it('Validate App Landed On Browse Jobs Section Page', () => {
    page.navigateTo();
    expect(page.getBrowseHeaderText()).toEqual('BROWSE JOBS');
    expect(page.getTechnologyHeaderText()).toEqual('TECHNOLOGY');
  });

  it('Validate Browse Jobs Section Page Display Atleast 1 Job', () => {
    expect(page.getJobBoardListCount()).toBeGreaterThan(0);
  });

  it('Validate Display Jobs Title/Location/Date Posted Not Empty', () => {
    page.validateJobTableCellNotEmpty();
  });

  it('Validate Each Displayed Job Applied Successfully', () => {
    page.validateEachJobAppliedSuccessfully();
  });

  afterEach(async () => {
    // Assert that there are no errors emitted from the browser
    const logs = await browser.manage().logs().get(logging.Type.BROWSER);
    expect(logs).not.toContain(jasmine.objectContaining({
      level: logging.Level.SEVERE,
    } as logging.Entry));
  });
});
