# Galytix QA Exercise

Dear candidate,

For the second phase of the interview process we would like to ask you to attempt a short assignment that would help us to evaluate your test-planning and automation skills.

You will be required to create a high level test-plan for this angular app.

We ask you to include the  test plan:

- The “Browse jobs” page
- All the pages behind the links in the central section of the page (Please note that the list of jobs changes randomly when reloading the page.  Please think about how to test an unknown number of items)  
- The job application process for each job advertised on the website. “Apply Now” button and the pop-up form. (Go ahead and submit the form, this is just a test app, the request doesn't go anywhere)

Once the test plan is ready please use protractor, or any other javascript automation testing framework you are comfortable with, to create tests. 

The created set of tests should be stateless and give consistent result which does not depend on the order in which the tests run. 

Your tests should be added to a public git repository with the instructions on how to run them.

Please aim to complete this exercise in three hours. 

Please share the link to the repository when the assignment is complete. 

We wish you best of luck.

# Instructions:

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 10.0.5.

Checkout or download the code, then install the Angular CLI, and do:

`npm install`

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Running end-to-end tests (QA Assignment)

Run `npm run e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

We have added tests in following file `e2e/src/app.e2e-spec.ts`

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).

## Improvement areas

We can provide unique attributes to application stable locator so that we can uniquely identify them

Further we can segregate the Framework structure to improve the re-usability and create re-usable POM, libraries etc

We can introduce the test-data generation capabilities which will take care of test data needs

Implement Cross-browser/platforms capabilities

CI-CD integration

